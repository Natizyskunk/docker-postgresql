# Use an official ubuntu os environment as a parent image
FROM ubuntu:18.04


# Install any needed packages for the container.
RUN apt update -y && apt upgrade -y && apt dist-upgrade -y

RUN apt install postgresql
RUN 

RUN apt install nginx
#  RUN sudo cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/localhost.conf
RUN cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup-original
RUN sudo mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.disabled
COPY ./localhost.conf /etc/nginx/conf.d/localhost.conf
RUN sudo nginx -t
RUN sudo nginx -s reload

RUN apt install php
#  RUN apt install php7.2 php7.2-common php7.2-cli php7.2-fpm
RUN php -v
RUN apt install php-pear php-fpm php-dev php-zip php-curl php-xmlrpc php-gd php-mysql php-mbstring php-xml libapache2-mod-php
RUN update-alternatives --set php /usr/bin/php7.2
RUN a2enmod php7.2
RUN systemctl restart apache2
RUN pecl install apcu
RUN echo "extension=apcu.so" | tee -a /etc/php/7.2/mods-available/cache.ini
#  RUN ln -s /etc/php/7.2/mods-available/cache.ini /etc/php/7.2/apache2/conf.d/30-cache.ini
RUN systemctl restart apache2


# Set the working directory to /app and copy root to /app container folder
WORKDIR /app
COPY . /app

# Install packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt
#  RUN pip-3.3 install -r requirements.txt
#  RUN pip-3.3 install .

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME myapp

# Set proxy server, replace host:port with values for your servers
#  ENV http_proxy host:port
#  ENV https_proxy host:port

# Run app.py when the container launches
CMD ["python", "app.py"]
## CMD myapp --port 8000